// Find Total Price Per Customer

Step 1:
/*

Define the map function to process each input document:

- In the function, this refers to the document that the map-reduce operation is processing.
- The function maps the price to the cust_id for each document and emits the cust_id and price pair.

*/

var mapPrice = function() {
    emit(this.cust_id, this.price);
 };


 Step 2:
 /*
 Define the corresponding reduce function with two arguments keyCustId and valuesPrices:

- The valuesPrices is an array whose elements are the price values emitted by the map function and grouped by keyCustId.
- The function reduces the valuesPrice array to the sum of its elements.

*/

var reducePrice = function(keyCustId, valuesPrices) {
    return Array.sum(valuesPrices);
 };


 Step 3:
 /*

 Perform map-reduce on all documents in the orders collection using the 
 - mapPrice map function and 
 - reducePrice reduce function

 */

db.orders.mapReduce(
    mapPrice,
    reducePrice,
    { out: "pricePerCustomer" }
 )

 