db.orders.aggregate([
    { $group: { _id: "$cust_id", value: { $sum: "$price" } } },
    { $out: "pricePerCustomer" }
 ])