const express = require('express');
const config = require('config');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

mongoose.connect('mongodb://localhost/AuthorsList');
const db = mongoose.connection;

db.on('connected', () => {
    console.log('DB connected successfully');
})

db.on('error', () => {
    console.log('Failed to connect DB');
})

const Authorschema = mongoose.Schema({
  name: {type: String, required: true},
  email: {type: String, required: true},
  mobileNo: {type: Number, required: true}
});

const Authormodel = mongoose.model('author', Authorschema);


const bookDataSchema = mongoose.Schema({
    title: {type:String, required:true, unique: true},
    author: {type:String, required:true},
    coverPage: {type:String, default: 'https://image.freepik.com/free-vector/comic-book-cover-page-template-design_1017-15145.jpg'},
    dataOfPublish: {type:Date, required:true}
});

const BookDataModel = mongoose.model('BookCollection', bookDataSchema);

// Books API calls
// GET API call
app.get('/books', (req, res, next) => {
    BookDataModel.aggregate([
        {
            $addFields: {"_ID": {$toObjectId: '$author'}}
        },
        {
            $lookup: {
                from: "authors",
                localField: "_ID",
                foreignField: "_id",
                as: "AuthorDetailsAttached"
            }
        }
    ], (err, result) => {
            if(err) {
                next();
            } else {
                res.json(result);
            }
        }
    );

    // BookDataModel.find({}, (err, result) => {
    //     if(err) {
    //         next();
    //     } else {
    //         res.json(result);
    //     }
    // })
})
// GET BY ID API call
app.get('/books/:id', (req, res, next) => {
    BookDataModel.findById(req.params.id, (err, result) => {
        if(err) {
            next();
        } else {
            res.json(result);
        }
    })
})

// POST API call
app.post('/books', (req, res, next) => {
    BookDataModel.create(req.body, (err, result) => {
        if(err) {
            next();
        } else {
            res.json(result);
        }
    })
})
// PUT API call
app.put('/books/:id', (req, res, next) => {
    BookDataModel.findByIdAndUpdate(req.params.id, req.body, (err, result) => {
        if(err) {
            next();
        } else {
            res.json(result);
        }
    })
})
// DELETE API call
app.delete('/books/:id', (req, res, next) => {
    BookDataModel.find(req.params.id, (err, result) => {
        if(err) {
            next();
        } else {
            res.json(result);
        }
    })
})


// Author API calls
// GET API call
app.get('/authors', (req, res, next) => {
    Authormodel.find({}, (err, result) => {
        if(err) {
            next();
        } else {
            res.json(result)
        }
    })
})
// GET BY ID API call
app.get('/authors/:id', (req, res, next) => {
    Authormodel.findById(req.params.id, (err, result) => {
        if(err) {
            next();
        } else {
            res.json(result)
        }
    })
    
})
// POST API call
app.post('/authors', (req, res, next) => {
    Authormodel.create(req.body, (err, result) => {
        if(err) {
            next();
        } else {
            res.json(result)
        }
    })
    
})
// PUT API call
app.put('/authors/:id', (req, res, next) => {
    Authormodel.findByIdAndUpdate(req.params.id, req.body, (err, result) => {
        if(err) {
            next();
        } else {
            res.json(result)
        }
    })
    
})
// DELETE API call
app.delete('/authors/:id', (req, res, next) => {
    Authormodel.findByIdAndDelete(req.params.id, (err, result) => {
        if(err) {
            next();
        } else {
            res.json(result)
        }
    })
    
})

app.use((err, req, res, next) => {
    console.log(err);
    res.json({message: err.message})
})

const PORT = config.get('PORT');
app.listen(PORT, () => {
    console.log(`Server listening Port ${PORT}`);
})