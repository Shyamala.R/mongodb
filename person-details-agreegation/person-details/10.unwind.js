// Unwind splits each document {with specified array} to serveral documents-one document per array element

/*
Let's understand,address field has array as datatype so unwind will take each element of the array and make it another  document.
If we have 4 rows and a field contains array of 3 element each, total fields wil be 4*3=12 out of three three element will have only on filed will differ that is field with array peroiviosly
*/

db.people.aggregate([
    {$unwind:"$address"},
    {$project:{name:1,index:1,tags:1}}
])

// No data as person.json does not have 


