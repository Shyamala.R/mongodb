db.people.aggregate([
    {$match:{age:{$gte:25}}},
    {$group:{_id:{eyeColor:"$eyeColor",age:"$age"}}},
    {$count:"eyeColorAndAge"}
])