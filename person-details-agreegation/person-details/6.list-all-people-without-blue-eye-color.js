db.people.aggregate([
    { $match: { eyeColor: { $ne: "blue" } } },
    {
        $group: {
            _id: {
                eyeColor: "$eyeColor",
                favoriteFruit: "$favoriteFruit"
            }
        }
    },
    { $sort: { "_id.eyeColor": 1, "_id.favoriteFruite": -1 } }
]) 

// Simple aggregation
db.people.aggregate([
    { $match: { eyeColor: { $ne: "blue" } } }
]) 