db.people.aggregate([
    {
        $group: {
            _id: "$favoriteFruit",
            avgAge: { $avg: "$age" }
        }
    }
])

db.people.aggregate([
    {
        $group: {
            _id: "$company.location.country",
            avgAge: { $avg: "$age" }
        }
    }
])
