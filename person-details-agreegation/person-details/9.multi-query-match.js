db.people.aggregate([
    //stage 0 
    { $limit: 100 },
    //stage 1
    { $match: { eyeColor: { $ne: "blue" } } },
    //stage 2
    {
        $group: {
            _id: {
                eyeColor: "$eyeColor",
                favoriteFruit: "$favoriteFruit"
            }
        }
    },
    //stage 3
    { $sort: { "_id.eyeColor": 1, "_id.favoriteFruit": -1 } }
])
