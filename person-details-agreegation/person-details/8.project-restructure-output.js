db.people.aggregate([
    {
        $project: {
            _id: 0,
            name: 1,
            extraMetaInfo: {
                eyes: "$eyeColor",
                company: "$company.title",
                country: "$company.location.country"
            }
        }
    }
])