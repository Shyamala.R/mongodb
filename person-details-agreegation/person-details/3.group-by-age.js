// List all people by age group
db.people.aggregate([
    {$group:{_id:"$age"}},
    {$count:"age"}
])