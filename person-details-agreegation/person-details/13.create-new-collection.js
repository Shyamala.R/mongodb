db.people.aggregate([
    { $group: { _id: { age: "$age", eyeColor: "$eyeColor" } } },
    { $out: "peopleAggregationAgeEyecolor" }    //if this collection does not exist mongodb will create one.
])