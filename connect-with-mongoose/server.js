const express = require('express');
const config = require('config');
const mongoose = require('mongoose')
const app = express();

mongoose.connect('mongodb://localhost/examples');
 const db = mongoose.connection
db.on('connected', () => {
    console.log('connected successfuly');
})

db.on('error', (err) => {
    console.log('failed to connect', + err);
})


const Schema = mongoose.Schema;
 
const BlogPostScheme = new Schema({
  title: String,
  body: String,
  date: Date
});
const BlogPost = mongoose.model('BlogPosts', BlogPostScheme);
BlogPost.create({title: "Shyamala"}, (err, result) => {
    console.log(err, result);
})




const PORT = config.get('PORT');
app.listen(PORT, () => {
    console.log(`Server listening port ${PORT}`);
})