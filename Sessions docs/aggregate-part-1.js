
// Products, categories, reviews
// Find reviews of a specific product
product = db.products.findOne({ 'slug': 'wheel-barrow-9092' })
reviews_count = db.reviews.count({ 'product_id': product['_id'] })

// summary of reviews for  all products
reviewCount = db.reviews.aggregate([
    {
        $group: {
            _id: '$product_id',
            count: { $sum: 1 }
        }
    }
]).next();
// Try with two stages



// rating summary - for selected product
// look up product first
product = db.products.findOne({ 'slug': 'wheel-barrow-9092' })

totalRating = db.reviews.aggregate([
    { $match: { product_id: product['_id'] } },
    {
        $group: {
            _id: '$product_id',
            count: { $sum: 1 }
        }
    }
]).next();
// Fix this aggregate to get "totalRating"


// Adding average review 

ratingSummary = db.reviews.aggregate([
    { $match: { 'product_id': product['_id'] } },
    {
        $group: {
            _id: '$product_id',
            average: { $avg: '$rating' },
            count: { $sum: 1 }
        }
    }
]).next();


// With group first

// below verifies that in fact, the first command will use an index, second will not

db.reviews.ensureIndex({ product_id: 1 })

countsByRating = db.reviews.aggregate([
    { $match: { 'product_id': product['_id'] } },
    {
        $group: {
            _id: '$rating',
            count: { $sum: 1 }
        }
    }
], { explain: true })

countsByRating = db.reviews.aggregate([
    {
        $group: {
            _id: { 'product_id': '$product_id', rating: '$rating' },
            count: { $sum: 1 }
        }
    },
    { $match: { '_id.product_id': product['_id'] } }
], { explain: true })


ratingSummary = db.reviews.aggregate([
    {
        $group: {
            _id: '$product_id',
            average: { $avg: '$rating' },
            count: { $sum: 1 }
        }
    },
    { $match: { '_id': product['_id'] } }
]).next();


// Counting Reviews by Rating
countsByRating = db.reviews.aggregate([
    { $match: { 'product_id': product['_id'] } },
    {
        $group: {
            _id: '$rating',
            count: { $sum: 1 }
        }
    }
]).toArray();

// Joining collections
db.products.aggregate([
    {
        $group: {
            _id: '$main_cat_id',
            count: { $sum: 1 }
        }
    }
]);




// findOne on mainCategorySummary

db.mainCategorySummary.findOne()

// Faster Joins - $unwind

// FASTER JOIN - $UNWIND
db.products.aggregate([
    { $project: { category_ids: 1 } },
    { $unwind: '$category_ids' },
    {
        $group: {
            _id: '$category_ids',
            count: { $sum: 1 }
        }
    },
    { $out: 'countsByCategory' }
]);

//  related findOne() - Using $out to create new collections
db.countsByCategory.findOne()

// $out and $project section

db.products.aggregate([
    {
        $group: {
            _id: '$main_cat_id',
            count: { $sum: 1 }
        }
    },
    { $out: 'mainCategorySummary' }
]);


db.products.aggregate([
    { $project: { category_ids: 1 } }
]);

//  User and Order
db.reviews.aggregate([
    {
        $group:
        {
            _id: '$user_id',
            count: { $sum: 1 },
            avg_helpful: { $avg: '$helpful_votes' }
        }
    }
])

