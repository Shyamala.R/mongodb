// import data 
mongoimport --db eShop --collection orders --drop --file orders.json
mongoimport --db eShop --collection products --drop --file products.json
mongoimport --db eShop --collection categories --drop --file categories.json
mongoimport --db eShop --collection reviews --drop --file reviews.json
mongoimport --db eShop --collection users --drop --file users.json
