db.zip.aggregate([
  { 
    "$group": {
      "_id": "$state",
      "population" : {
        "$sum": "$pop"
      }
    }
  }
])