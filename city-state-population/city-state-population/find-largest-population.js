// Return Largest and Smallest Cities by State

db.zip.aggregate([
    { "$group": { "_id": { "state": "$state", "city": "$city" }, "city_pop": { "$sum": "$pop" } } },
    { "$sort": { "city_pop": 1 } }
])

// todo: fix the query to get the max populated city & least populated city
// $min, $max