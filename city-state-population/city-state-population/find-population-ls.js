// Largest and Smallest Cities by State: return the first and last city name and population 

db.zip.aggregate([{
    '$group': {
        '_id': { 'city': '$city', 'state': '$state' },
        'city_pop': { '$sum': '$pop' }
    }
},
{ '$sort': { 'city_pop': 1 } },
{
    '$group': {
        '_id': '$_id.state',
        'biggestCity': { '$last': '$_id.city' },
        'biggestPop': { '$last': '$city_pop' },
        'smallestCity': { '$first': '$_id.city' },
        'smallestPop': { '$first': '$city_pop' }
    }
},
{
    '$project': {
        '_id': 0,
        'biggestCity': { 'name': '$biggestCity', 'pop': '$biggestPop' },
        'smallestCity': { 'name': '$smallestCity', 'pop': '$smallestPop' },
        'state': '$_id'
    }
}])

