db.zip.aggregate([
      { $group:
        {
          _id: { state: "$state", city: "$city" },
          pop: { $sum: "$pop" },
          totalCity: { $sum: 1 }
        }
      },
      { $sort: { totalCity: 1 } },
      { $group:
        {
          _id : "$_id.state",
          biggestCity:  { $last: "$_id.city" },
          biggestPop:   { $last: "$pop" },
          smallestCity: { $first: "$_id.city" },
          smallestPop:  { $first: "$pop" },
          totalCity: { $sum: "$totalCity" }
        }
      },
      { $project:
        { _id: 0,
          state: "$_id",
          totalCity: "$totalCity",
          biggestCity:  { name: "$biggestCity",  pop: "$biggestPop" },
          smallestCity: { name: "$smallestCity", pop: "$smallestPop" },
        }
      }
    ])