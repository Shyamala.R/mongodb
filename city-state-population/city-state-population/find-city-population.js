// Return Average Top 10 City Population  by State

db.zip.aggregate([
    { "$group": { "_id": { "state": "$state", "city": "$city" }, "city_pop": { "$sum": "$pop" } } },
    { "$group": { "_id": "$_id.state", "avgCityPop": { "$avg": "$city_pop" } } },
    { "$sort": { "avgCityPop": -1 } },
    { "$limit": 10 }
])