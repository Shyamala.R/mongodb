// Return States with Populations above 10 Million

db.zip.aggregate([
    { "$group": { "_id": "$state", "totalPop": { "$sum": "$pop" } } },
    { "$match": { "totalPop": { "$gte": 10 * 1e6 } } }
])


